import requests
import products.constants as c
from django.db import IntegrityError

from products.models import Product, Category
from django.core.management.base import BaseCommand
from django.db.utils import DataError


class Command(BaseCommand):
    """ Class to handle the database """

    def add_arguments(self, parser):
        parser.add_argument(
            '--categories',
            type=int,
            default=500,
            help="Number of categories fetch by default from the API"
        )
        parser.add_argument(
            '--products',
            type=int,
            default=500,
            help="Number of products to fetch per category from the API"
        )

    @staticmethod
    def clean_db():
        """ Clear database of all data"""
        Category.objects.all().delete()
        Product.objects.all().delete()

    def categories_db(self):
        """ Creation of the categories' table and insertion of
            categories from OpenFoodFacts Api
        """
        products = self.fetch_products()
        for prod in products:
            categories = prod['categories'].split(', ')
            for category in categories:
                try:
                    Category.objects.bulk_create(
                        [
                            Category(category_name=category)
                        ]
                    )
                except IntegrityError:
                    return "No category name available"

    @staticmethod
    def fetch_products():
        """ Method to fetch the products sorted by some parameters """

        categories = c.CATEGORIES_LIST
        products = []
        for category in categories:
            parameters = {
                "action": "process",
                "tagtype_0": "categories",
                "tag_contains_0": "contains",
                "tag_0": f"{category}",
                "page_size": 30,
                "sort_by": "unique_scan_n",
                "json": 1
            }
            r = requests.get(c.URL, parameters)
            data = r.json()["products"]
            for product in data:
                product["categories"] = f"{category}, {product['categories']}"
                products.append(product)
        return products

    def clean_data(self, products):
        """ Method to delete all the products that
            don't have the specifications we want """

        return [element for element in products if self.is_valid(element)]

    @staticmethod
    def is_valid(product):
        # sourcery skip: merge-duplicate-blocks, remove-redundant-if
        """ Method that specify what is valid or not """
        for tag in c.TAGS:
            if tag not in product:
                return False
            else:
                if not product[tag]:
                    return False
        for prod in c.KEEP_DATA_NUTRIMENTS:
            if prod not in product['nutriments']:
                return False
            else:
                if not product['nutriments'][prod]:
                    return False
        for prod in c.KEEP_DATA_NUTRIMENTS_OPTIONS:
            if prod not in product['nutrient_levels']:
                return False
            else:
                if not product['nutrient_levels'][prod]:
                    return False

        return True

    @staticmethod
    def save_data(products):
        """ Method to save products in the products' table """
        try:
            for data in products:
                product = Product.objects.create(
                    product_name=data['product_name'],
                    nutrition_grades=data['nutrition_grade_fr'],
                    fat=data['nutriments'].get('fat'),
                    fat_100g=data['nutriments'].get('fat_100g'),
                    saturated_fat=data['nutriments'].get('saturated-fat'),
                    saturated_fat_100g=data['nutriments'].get(
                        'saturated-fat_100g'
                    ),
                    sugars=data['nutriments'].get('sugars'),
                    sugars_100g=data['nutriments'].get('sugars_100g'),
                    salt=data['nutriments'].get('salt'),
                    salt_100g=data['nutriments'].get('salt_100g'),
                    image_url=data['image_url'],
                    url=data['url'],
                )
                categories = []
                for cat in data['categories'].split(', '):
                    category, created = Category.objects.get_or_create(
                        category_name=cat
                    )
                    categories.append(category)
                product.categories.add(*categories)
        except DataError:
            pass

    def handle(self, *args, **options):
        """method to launch all the others methods"""
        self.clean_db()
        self.categories_db()
        products_list = self.fetch_products()
        clean_prod = self.clean_data(products_list)
        self.save_data(clean_prod)
        self.stdout.write(self.style.SUCCESS("Done"))
